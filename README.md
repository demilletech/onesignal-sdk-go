# Go client for the OneSignal API

Forked from https://github.com/tbalthazar/onesignal-go

This library provides a simple SDK to access the [OneSignal API](https://documentation.onesignal.com/docs/server-api-overview).

## Installation

```bash
go get gitlab.com/demilletech/onesignal-sdk-go
```

## Usage

```go
import "gitlab.com/demilletech/onesignal-sdk-go"
```

## Documentation

Full usage and examples, see the [package docs](https://godoc.org/gitlab.com/demilletech/onesignal-sdk-go)

## Changes

See [CHANGELOG.txt](CHANGELOG.txt)

## Contributors

Original contributors to tbalthazar's repository are [listed here](https://github.com/tbalthazar/onesignal-go/graphs/contributors).

## License

Please see [LICENSE](/LICENSE) for licensing details. 
